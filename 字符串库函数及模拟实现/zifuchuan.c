#define _CRT_SECURE_NO_WARNINGS 1
//strlen的使用
//#include<stdio.h>
//#include<string.h>
//int main()
//{
//	char arr[] = { "abcd" };
//	int sum = strlen(arr);
//	printf("%d\n", sum);
//	return 0;
//}


//strlen的模拟实现
//1.使用计数器
//#include<stdio.h>
//#include<string.h>
//#include<assert.h>
//size_t my_strlen(const char* str)
//{
//	int count = 0;
//	assert(str);
//	while (*str)
//	{
//		str++;
//		count++;
//	}
//	return count;
//}
//int main()
//{
//	char arr[] = "abcd";
//	int sum=my_strlen(arr);
//	printf("%d", sum);
//	return 0;
//}

//2.指针减去指针
//#include<stdio.h>
//#include<string.h>
//#include<assert.h>
//size_t my_strlen(const char* str)
//{
//	assert(str);
//	char* p = (char*)str;
//	while (*p != '\0')
//	{
//		p++;
//	}
//	return p - str;
//}
//int main()
//{
//	char arr[] = { "abcde" };
//	int sum = my_strlen(arr);
//	printf("%d", sum);
//	return 0;
//}

//#include<stdio.h>
//#include<string.h>
//#include<assert.h>
//size_t my_strlen(const char* str)
//{
//	assert(str);
//	if (*str == '\0')
//	{
//		return 0;
//	}
//	else
//		return 1 + my_strlen(str + 1);
//}
// int main()
//{
//	char arr[] = { "abcde" };
//	int sum = my_strlen(arr);
//	printf("%d", sum);
//	return 0;
//}

//2.strcpy
//#include<stdio.h>
//#include<string.h>
//int main()
//{
//	char arr1[] = "hello world";
//	char arr2[20] = { 0 };
//	strcpy(arr2, arr1);
//	printf("%s", arr2);
//	return 0;
//}

//#include<stdio.h>
//#include<string.h>
//void my_strcpy(char* dest, const char* src)
//{
//	while (*src != '\0')
//	{
//		*dest = *src;
//		src++;
//		dest++;
//	}
//	*dest = *src;
//}
//int main()
//{
//	char arr1[] = { "hello word" };
//	char arr2[20] = { 0 };
//	my_strcpy(arr2, arr1);
//	printf("%s", arr2);
//	return 0;
//}

//#include<stdio.h>
//#include<string.h>
//#include<assert.h>
//char* my_strcpy(char* dest, const char* src)
//{
//	assert(dest && src);
//	char* ret = dest;
//	while (*dest++ = *src++)
//	{
//		;
//	}
//	return ret;
//}
//int main()
//{
//	char arr1[] = { "hello word" };
//	char arr2[20] = { 0 };
//	my_strcpy(arr2, arr1);
//	printf("%s", arr2);
//	return 0;
//}


//strcat的使用
//#include<stdio.h>
//#include<string.h>
//int main()
//{
//	char arr1[20] = { "hello " };
//	char arr2[] = { "world" };
//	strcat(arr1, arr2);
//	printf("%s", arr1);
//	return 0;
//}

//strcat的模拟实现
//#include<stdio.h>
//#include<string.h>
//#include<assert.h>
//char* my_strcat(char* dest, const char* src)
//{
//	assert(dest && src);
//	char* ret = dest;
//	//1.寻找目标字符串的\0
//	while (*dest != '\0')
//	{
//		dest++;
//	}
//	//2.拷贝字符串到目标空间
//	while (*dest++ = *src++)
//	{
//		;
//	}
//	return ret;
//}
//int main()
//{
//	char arr1[20] = { "hello " };
//	char arr2[] = { "world" };
//	my_strcat(arr1, arr2);
//	printf("%s", arr1);
//	return 0;
//}

//strcmp的使用
//#include<stdio.h>
//#include<string.h>
//int main()
//{
//	char arr1 []= { "abcd" };
//	char arr2 []= { "abbd" };
//	int ret = strcmp(arr1, arr2);
//	printf("%d", ret);
//	return 0;
//}

//strcmp的模拟实现
//#include<stdio.h>
//#include<string.h>
//#include<assert.h>
//int my_strcmp(const char* str1, const char* str2)
//{
//	assert(str1 && str2);
//	while (*str1 == *str2)
//	{
//		if (*str2 == '\0')
//		{
//			return 0;
//		}
//		str2++;
//		str1++;
//	}
//	return *str1 - *str2;
//}
//int main()
//{
//	char arr1[] = { "abcd" };
//	char arr2[] = { "abd" };
//	int ret = my_strcmp(arr1, arr2);
//	printf("%d", ret);
//	return 0;
//}

//strncpy的使用
//#include<stdio.h>
//#include<string.h>
//int main()
//{
//	char arr1[] = { "abcd" };
//	char arr2[] = { "erd" };
//	strncpy(arr2, arr1, 2);
//	printf("%s", arr2);
//	return 0;
//}


//strncat的使用
//#include<stdio.h>
//#include<string.h>
//int main()
//{
//	char arr1[20] = { "hello " };
//	char arr2[] = { "worldxx" };
//	strncat(arr1, arr2, 5);
//	printf("%s", arr1);
//	return 0;
//}

//strncmp的使用
//#include<stdio.h>
//#include<string.h>
//int main()
//{
//	char arr1[] = { "abdc" };
//	char arr2[] = { "bcdc" };
//	int ret=strncmp(arr1, arr2, 2);
//	printf("%d", ret);
//	return 0;
//}


//strstr的使用
//#include<stdio.h>
//#include<string.h>
//int main()
//{
//	char arr1[] = { "this is a flower" };
//	char arr2[] = { "is" };
//	char* ret=strstr(arr1, arr2);
//	printf("%s", ret);
//	return 0;
//}


//模拟实现strstr
//#include<stdio.h>
//#include<string.h>
//char* my_strstr(const char* str1, const char* str2)
//{
//	const char* s1 = NULL;
//	const char* s2 = NULL;
//	const char* cur = str1;
//	if (*str2 == '\0')
//	{
//		return (char*)str1;
//	}
//	while (*cur)
//	{
//		s1 = cur;
//		s2 = str2;
//		while (*s1 != '\0' && *s2 != '\0' && *s1 == *s2)
//		{
//			s1++;
//			s2++;
//		}
//		if (*s2 == '\0')
//		{
//			return cur;
//		}
//		cur++;
//	}
//	return NULL;
//}
//int main()
//{
//	char arr1[] = { "this is a flower" };
//	char arr2[] = { "is" };
//	char* ret = my_strstr(arr1, arr2);
//	if (ret != NULL)
//	{
//		printf("%s\n", ret);
//	}
//	else
//		printf("找不到\n");
//	
//	return 0;
//}


//strncpy模拟实现
//#include<stdio.h>
//#include<string.h>
//#include<assert.h>
//char* my_strncpy(char* dest, const char* src,size_t sz)
//{
//	assert(dest && src);
//	char* ret = dest;
//	while (sz--)
//	{
//		*dest++ = *src++;
//	}
//	return ret;
//}
//int main()
//{
//	char arr1[] = { "hello world" };
//	char arr2[20] = { 0 };
//	my_strncpy(arr2, arr1,5);
//	printf("%s", arr2);
//	return 0;
//}


//strncat的模拟实现
//#include<stdio.h>
//#include<string.h>
//#include<assert.h>
//char* my_strncat(char* dest, const char* src,size_t sz)
//{
//	assert(dest && src);
//	char* ret = dest;
//	//1.寻找目标字符串的\0
//	while (*dest != '\0')
//	{
//		dest++;
//	}
//	//2.拷贝字符串到目标空间
//	while (sz--)
//	{
//		*dest++ = *src++;
//	}
//	*dest='\0';
//	return ret;
//}
//int main()
//{
//	char arr1[20] = { "hello " };
//	char arr2[] = { "world" };
//	my_strncat(arr1, arr2,2);
//	printf("%s", arr1);
//	return 0;
//}

//#include<stdio.h>
//#include<string.h>
//#include<assert.h>
//int my_strncmp(const char* str1, const char* str2,size_t sz)
//{
//	assert(str1 && str2);
//	while (*str1 == *str2 && sz!=1)
//		//如果输入1的话就是字符了，而不是字符串
//	{
//		if (*str2 == '\0')
//		{
//			return 0;
//		}
//		sz--;
//		str2++;
//		str1++;
//	}
//	return *str1 - *str2;
//}
//int main()
//{
//	char arr1[] = { "abcd" };
//	char arr2[] = { "abd" };
//	int ret = my_strncmp(arr1, arr2,2);
//	printf("%d", ret);
//	return 0;
//}